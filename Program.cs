﻿using System;
using System.IO;
using System.Diagnostics;

#pragma warning disable CS8602

namespace automate
{
    class Program
    {
        public static string Version = "0.1.1";
        public static string Build = "2052622";
        public string? WSHUserName = Environment.UserName;

        public static void Main()
        {
            var xp = new Program();
            string? path = @"c:\";
            Console.Write("{0}> ", xp.WSHUserName);
            string? action = Console.ReadLine();
            if (action.StartsWith("echo"))
            {
                string text = action.Remove(0, 5);
                Console.WriteLine(text);
                Main();
            }
            else if (action.StartsWith("wrsh"))
            {
                string scriptName = action.Remove(0, 5);
                wrsh.Init(scriptName);
                Main();
            }
            else if (action.StartsWith("batch"))
            {
                string scriptName = action.Remove(0, 6);
                Extra.ExecuteCommand(scriptName);
            }
            else
            {
                switch (action)
                {
                    case "":
                        break;
                    case "clear":
                        Console.Clear();
                        break;
                    case "wshuch":
                        Console.Write("> ");
                        string? action2 = Console.ReadLine();
                        xp.WSHUserName = action2;
                        break;
                    case "version":
                        Console.WriteLine("Version: {0}", Version);
                        Console.WriteLine("Build: {0}", Build);
                        break;
                    case "exit":
                        Environment.Exit(0);
                        break;
                    case "ls":
                        string[] filesArray = Directory.GetFiles(path);
                        Console.WriteLine(filesArray);
                        break;
                    default:
                        try
                        {
                            string[] actionaa = action.Split(" ");
                            string[] actionab = action.Split(" ");
                            actionab[0] = String.Empty;
                            string arg = string.Join(" ", actionab);
                            var proccess = Process.Start(actionaa[0], arg);
                            proccess.WaitForExit();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("(!) {0}", e.Message);
                            Console.WriteLine("(?) It seems that there isn't any commands like this. Check Command's Grammar. Your Command: \"{0}\".", action);
                        }
                        break;
                }
                Main();
            }
        }
    }
    // class Extra is needed for extra stuff
    class Extra
    {
        public static void showHelp()
        {
            Console.WriteLine("-------Help-------");
            Console.WriteLine("echo - displays text after \"echo\". \nversion - displays version. \nexit - exit the shell. \nls - shows files in local directory.");
        }
        public static void ExecuteCommand(string command)
        {
            try
            {
            int exitCode;
            ProcessStartInfo? processInfo;
            Process? process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Program.Main();
            }
        }
    }
    // class wrsh is needed for making possible scripts in this program.
    class wrsh
    {
        // Init is main void of the class, with which we open and run wreshscripts.
        static public void Init(string sN)
        {
            // rn placeholder
        }
    }
}